import datetime
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


from .member import Member


class Case(models.Model):
    """
    Possible fields:
    * Employer information (Could alternatively be saved in a note)
      * Employer contact info
    * Date of last note property
    * date_closed, set when making status closed
    * Custom case name
    * Type of case: Greivance, Performance Sancion
    """

    class Status(models.TextChoices):
        OPEN = "OPEN", _("Open")
        CLOSED = "CLOSED", _("Closed")

    case_number = models.AutoField(primary_key=True, editable=False)
    status = models.CharField(
        max_length=10, choices=Status.choices, default=Status.OPEN
    )

    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    reps_on_case = models.ManyToManyField(
        User,
        blank=True,
        help_text="Optional. It's not required to assign reps when opening a case.",
    )

    case_name = models.CharField(
        max_length=64,
        blank=True,
        null=True,
        help_text="Optional. Use to set a custom case name if desired. If blank, member name will be shown.",
    )

    member_contacted = models.BooleanField(
        default=False,
        help_text="Has this member been contacted by a rep yet? Check this box when the member is contacted.",
    )

    date_opened = models.DateField(default=datetime.date.today, null=False, blank=False)
    date_closed = models.DateField(null=True, blank=True)

    def __str__(self):
        case_text = self.member
        if self.case_name:
            case_text = self.case_name
        return "Case #{} - {}".format(self.case_number, case_text)


@receiver(pre_save, sender=Case)
def set_date_closed(sender, instance, **kwargs):
    try:
        current_case_state = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass
    else:
        if (
            current_case_state.status == Case.Status.OPEN
            and instance.status == Case.Status.CLOSED
        ):
            # Set date_closed when closing.
            instance.date_closed = datetime.date.today()
        elif (
            current_case_state.status == Case.Status.CLOSED
            and instance.status == Case.Status.OPEN
        ):
            # Remove date_closed value when reopening.
            instance.date_closed = None
