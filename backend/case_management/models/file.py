from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from .case import Case


class File(models.Model):
    class FileTypes(models.TextChoices):
        CONTRACT = "CONTRACT", _("Contract")
        WORKPLACE_PROCCESS = "WORKPLACE_PROCESS", _("Workplace Process Docs")
        NOTES = "NOTES", _("Notes")
        OTHER = "OTHER", _("Other")

    created_at = models.DateTimeField(default=now, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now=True)

    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    file_type = models.CharField(
        max_length=20, choices=FileTypes.choices, default=FileTypes.OTHER
    )

    file = models.FileField(
        max_length=256, blank=False, null=False, upload_to="uploaded_files"
    )

    file_notes = models.TextField(blank=True, null=True, help_text="Optional")

    def __str__(self):
        return self.file.name.split("/")[-1]
