from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from .case import Case


class Note(models.Model):
    class NoteTypes(models.TextChoices):
        GENERAL = "GENERAL", _("General")
        EMPLOYER_MEETING = "EMPLOYER_MEETING", _("Employer Meeting")
        MEMBER_MEETING = "MEMBER_MEETING", _("Member Meeting")

    created_at = models.DateTimeField(default=now, null=False, blank=False)
    last_modified = models.DateTimeField(auto_now=True)

    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    note_type = models.CharField(
        max_length=20, choices=NoteTypes.choices, default=NoteTypes.GENERAL
    )
    note_name = models.CharField(max_length=128)

    note_body = models.TextField()

    def __str__(self):
        return self.note_name
