from django.db import models


class Member(models.Model):
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    member_number = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone_number = models.CharField(max_length=30, blank=True, null=True)
    discord_username = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
