from django.http import HttpResponse

from rest_framework.decorators import api_view, permission_classes
from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication,
)
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import File


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def serve_file(request, pk):
    """Serve files in `uploaded_files` only to authenticated users"""
    selectedFile = File.objects.get(id=pk)
    if selectedFile:
        response = HttpResponse()
        response.content = selectedFile.file.read()
        response["Content-Disposition"] = "attachment; filename={0}".format(
            selectedFile
        )
        return response
    else:
        print("File not found")
