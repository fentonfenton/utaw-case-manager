from django.contrib import admin
from django.utils.html import format_html

from .models import Member, Case, Note, File


# Member Admin
class MemberAdmin(admin.ModelAdmin):
    list_display = ("__str__", "email", "phone_number", "cases")
    search_fields = (
        "first_name",
        "last_name",
        "discord_username",
        "email",
        "member_number",
    )

    def cases(self, obj: Member):
        """
        Link to cases page, showing cases for this user
        """
        return format_html(
            '<a href="/admin/case_management/case/?member__id__exact={}">{}\'s Cases</a>',
            obj.id,
            obj.first_name,
        )


admin.site.register(Member, MemberAdmin)

# Case Admin
class CaseAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "case_member",
        "status",
        "assigned_reps",
        "member_contacted",
        "case_notes",
        "case_files",
        "date_opened",
        "date_closed",
    )
    readonly_fields = ("date_opened", "date_closed")
    list_filter = ("status", "member_contacted", "member")
    search_fields = (
        "case_number",
        "member__first_name",
        "member__last_name",
    )
    ordering = ["-status", "case_number"]

    def case_member(self, obj: Case):
        """
        Link to the member's detail view page
        """
        return format_html(
            '<a href="/admin/case_management/member/{}/">{}</a>',
            obj.member.id,
            obj.member.__str__(),
        )

    def assigned_reps(self, obj: Case):
        """
        List of reps on case
        """
        rep_list = ",".join([rep.username for rep in obj.reps_on_case.all()])
        return rep_list

    def case_notes(self, obj: Case):
        """
        Link to notes page, showing notes for this case
        """
        return format_html(
            '<a href="/admin/case_management/note/?case__case_number__exact={}">Notes</a>',
            obj.case_number,
        )

    def case_files(self, obj: Case):
        """
        Link to files page, showing files for this case
        """
        return format_html(
            '<a href="/admin/case_management/file/?case__case_number__exact={}">Files</a>',
            obj.case_number,
        )


admin.site.register(Case, CaseAdmin)

# Note Admin
class NoteAdmin(admin.ModelAdmin):
    list_display = (
        "note_name",
        "case",
        "note_type",
        "created_at",
        "last_modified",
    )
    readonly_fields = ("created_at",)
    list_filter = ("note_type", "case")
    search_fields = (
        "note_name",
        "case__case_number",
        "case__member__first_name",
        "case__member__last_name",
    )
    ordering = ["-last_modified"]


admin.site.register(Note, NoteAdmin)

# File Admin
class FileAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "case",
        "file_type",
        "created_at",
        "last_modified",
        "download_file",
    )
    readonly_fields = ("created_at",)
    list_filter = ("file_type", "case")
    search_fields = (
        "case__case_number",
        "case__member__first_name",
        "case__member__first_name",
    )
    ordering = ["-last_modified"]

    def download_file(self, obj: File):
        """
        Button to download the file
        """
        return format_html(
            '<a href="{}">Download File</a>',
            "/serve_file/{}".format(obj.id),
        )


admin.site.register(File, FileAdmin)
