# UTAW Casework Manager

[Django Backend](./backend)

## Running locally for development using docker-compose

Requires:

- Docker
- docker-compose

To start the dev environment with docker for the first time:

1. Update your hosts file at `/etc/hosts` to map `django` to 127.0.0.1

2. Run the following code in the terminal.

```sh
# Build and start
docker-compose up --build -d

# See the logs for all services
docker-compose logs -f

# Migrations
docker-compose exec django python manage.py migrate

# Create a superuser for Django Admin
docker-compose exec django python manage.py createsuperuser
```

**The app will be running at `localhost:8000`** Access the admin interface at `localhost:8000/admin` where you can sign in with the superuser account.

### Making migrations

Whenever migrations are needed you can run:

```sh
docker-compose exec django python manage.py managemigrations <app>
docker-compose exec django python manage.py migrate <app>
```
